angular
    .module("angularApp")
    .controller("HomeCtrl", function($scope) {
        $scope.myInput = 'Init';

        $scope.imgUrl = "src/assets/sample.jpg";

        $scope.progress = 'Not clicked';

        $scope.onClick = function() {
            $scope.progress = 'Clicked'
        }

        $scope.showTable = true;
        $scope.show = function() {
            $scope.showTable = true;
        }

        $scope.hide = function() {
            $scope.showTable = false;
        }

        $scope.showLabel = 'Show';
        $scope.hideLabel = 'Hide';

        $scope.btnLabel = 'MyButton'
    });