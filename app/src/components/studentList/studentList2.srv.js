angular
    .module("angularApp")
    .factory("studentList2Service", function($http) {
        return {
            populateUsers: function() {
                return $http
                    .get('http://localhost:8888/users');
            },

            addUser: function(data) {
                return $http
                    .post('http://localhost:8888/users/', data);
            },

            updateUser: function(userId, data) {
                return $http
                    .put('http://localhost:8888/users/' + userId, data);
            },

            deleteUser: function(userId) {
                return $http
                    .delete('http://localhost:8888/users/' + userId);
            }
        };
    });