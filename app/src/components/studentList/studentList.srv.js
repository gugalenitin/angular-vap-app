(function(){
    var studentListServiceFn = function($http) {
        this.populateUsers = function() {
            return $http
                .get('https://jsonplaceholder.typicode.com/users');
        }
    };
    angular
        .module("angularApp")
        .service("studentListService", studentListServiceFn);
})();