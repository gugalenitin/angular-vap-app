angular
    .module('angularApp')
    .controller('StudentListCtrl', function($scope, $log, studentList2Service) {
        $scope.students = [
            { id: 1, name: 'Sachin', department: 'Comp', marks: [50, 50], doj: new Date() },
            { id: 2, name: 'Virat', department: 'IT', marks: [33, 55], doj: new Date() },
            { id: 3, name: 'Anil', department: 'Mech', marks: [22, 34], doj: new Date() },
            { id: 4, name: 'Mahesh', department: 'Civil', marks: [53, 23], doj: new Date() },
            { id: 5, name: 'Ramesh', department: 'Comp', marks: [43, 64], doj: new Date() }
        ];

        $scope.users = [];

        var fetchUsers = function() {
            studentList2Service
                .populateUsers()
                .then(function(response) {
                    if (response.status === 200) {
                        $scope.users = response.data.data;
                        $log.info('successful!');
                        $log.debug(response);
                    }
                }, function(error) {
                    console.log(error);
                });
        }

        fetchUsers();

        $scope.user = {
            name: undefined,
            email: undefined
        };

        $scope.addUser = function() {
            studentList2Service
                .addUser($scope.user)
                .then(function(){
                    fetchUsers();   
                }, function(error) {
                    console.log(error);
                });
        }

        $scope.updateUser = function(user) {
            studentList2Service
                .updateUser(user.id, user)
                .then(function(){
                    fetchUsers();   
                }, function(error) {
                    console.log(error);
                });
        }

        $scope.deleteUser = function(user) {
            if (window.confirm('Are you sure?')) {
                studentList2Service
                    .deleteUser(user.id)
                    .then(function(){
                        fetchUsers();   
                    }, function(error) {
                        console.log(error);
                    });
            }
        }
    });