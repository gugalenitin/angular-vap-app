(function(){
    angular
        .module('angularApp')
        .filter('square', function() {
            return function(num, multiple) {
                var result = 1;
                for (i = 0; i < multiple; i++) {
                    result = result * num;
                }
                return result;
            }
        });
})();