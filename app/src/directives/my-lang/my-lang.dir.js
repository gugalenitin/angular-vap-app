angular
    .module('angularApp')
    .directive('myLang', function() {
        return {
            templateUrl: 'src/directives/my-lang/my-lang.tpl.html',
            restrict: 'EA',
            scope: {
                myVal: '=myVal'
            }
        }
    });