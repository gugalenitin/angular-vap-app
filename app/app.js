angular.module("angularApp", ['ui.router']);

angular
    .module("angularApp")
    .config(function($locationProvider, $stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('homeState', {
                url: '/',
                controller: 'HomeCtrl',
                templateUrl: 'src/components/home/home.tpl.html'
            })
            .state('studentListState', {
                url: '/students',
                controller: 'StudentListCtrl',
                templateUrl: 'src/components/studentList/studentList.tpl.html'
            });

        $urlRouterProvider.otherwise('/');
    });