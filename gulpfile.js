var gulp = require('gulp');
var connect = require('gulp-connect');

var connectFn = function () {
  connect.server({
    root: 'app/',
    port: 8080
  });
}

gulp.task('connect', connectFn);